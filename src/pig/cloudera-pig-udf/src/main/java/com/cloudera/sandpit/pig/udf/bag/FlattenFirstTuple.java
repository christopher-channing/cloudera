// Copyright 2013 Cloudera Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.cloudera.sandpit.pig.udf.bag;

import java.io.IOException;

import org.apache.pig.Algebraic;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

/**
 * This Pig UDF is responsible for picking the first tuple within in a bag and flattening the 
 * output. This should only be used when a prior group by has been performed and the result
 * groups (bags) need to be limited.  
 * 
 * Note: Currently the Pig optimizer is not advanced enough to use a combiner within the overall
 * query plan when a "LIMIT" is used inside of a nested plan i.e FOREACH. Therefore this function
 * will trick the optimizer by pretending to be algebraic. The downside of this is that the result
 * of the UDF must be a scaler value, in this case a string based value.  
 */
public class FlattenFirstTuple extends EvalFunc<String> implements Algebraic
{	
	private static final String TUPLE_CLASS = FirstTuple.class.getName();
	
	private static final FirstTuple FIRST_TUPLE = new FirstTuple();
	private static final FirstTupleFinal FIRST_TUPLE_FINAL = new FirstTupleFinal();
	
	/*
	 * (non-Javadoc)
	 * @see org.apache.pig.Algebraic#getInitial()
	 */
	public String getInitial() 
	{
		return TUPLE_CLASS;
	}

	/*
	 * (non-Javadoc)
	 * @see org.apache.pig.Algebraic#getIntermed()
	 */
	public String getIntermed() 
	{
		return TUPLE_CLASS;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.apache.pig.Algebraic#getFinal()
	 */
	public String getFinal() 
	{
		return FirstTupleFinal.class.getName();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.apache.pig.EvalFunc#exec(org.apache.pig.data.Tuple)
	 */
	@Override
	public String exec(Tuple input) throws IOException 
	{
		return FIRST_TUPLE_FINAL.exec(FIRST_TUPLE.exec(input));
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.apache.pig.EvalFunc#outputSchema(org.apache.pig.impl.logicalLayer.schema.Schema)
	 */
    @Override
    public Schema outputSchema(Schema input) 
    {
        return new Schema(new Schema.FieldSchema(null, DataType.CHARARRAY)); 
    }
    
    public static class FirstTuple extends EvalFunc<Tuple>
    {
		@Override
		public Tuple exec(Tuple input) throws IOException 
		{
			Tuple result = null;
			
			if (input != null)
			{
				DataBag bag = (DataBag)input.get(0);
				
				if ((bag != null) && (bag.size() > 0))
				{
					result = bag.iterator().next();
				}
			}
			
			return result;
		} 
    }
        
	public static final class FirstTupleFinal extends EvalFunc<String>
	{
		@Override
		public String exec(Tuple input) throws IOException 
		{
			String result = null;
			
			if (input != null)
			{
				DataBag group = ((DataBag)input.get(0));
				
				if ((group != null) && (group.size() > 0))
				{
					Tuple tuple = group.iterator().next();
					
					if ((tuple != null) && (tuple.size() > 1))
					{
						result = tuple.get(1).toString();
					}
				}
				
			}
			
			return result;
		}
	}
}
